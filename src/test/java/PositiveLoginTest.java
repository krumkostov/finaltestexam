import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class PositiveLoginTest {
    WebDriver driver;


    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://shop.pragmatic.bg");
    }

    @AfterMethod
    public void tearDown(){
        driver.quit();

    }

    @Test
    public void UserPassLogin() throws InterruptedException {


        driver.findElement(By.cssSelector("li.dropdown a.dropdown-toggle")).click();

        driver.findElement(By.linkText("Register")).click();

        Assert.assertEquals(driver.findElement(By.linkText("Register")).getText(), "Register");

        WebElement firstName = driver.findElement(By.id("input-firstname"));
        firstName.sendKeys("Krum");


        WebElement lastName = driver.findElement(By.id("input-lastname"));
        lastName.sendKeys("Kostov");


        WebElement eMail = driver.findElement(By.id("input-email"));
        eMail.sendKeys("krum33@mail.bg");

        WebElement phone = driver.findElement(By.id("input-telephone"));
        phone.sendKeys("0888");

        WebElement pass = driver.findElement(By.id("input-password"));
        pass.sendKeys("123456");

        WebElement passRepeat = driver.findElement(By.id("input-confirm"));
        passRepeat.sendKeys("123456");


        WebElement subscribe = driver.findElement(By.cssSelector("label.radio-inline input"));

        WebElement con = driver.findElement(By.name("agree"));
        con.click();


        if (!subscribe.isSelected()) {
            subscribe.click();
        }
        Assert.assertTrue(subscribe.isSelected());

        WebElement continueButton = driver.findElement(By.cssSelector("input.btn"));
        continueButton.click();

        Assert.assertEquals(driver.findElement(By.linkText("Logout")).getText(), "Logout");
    }
}

